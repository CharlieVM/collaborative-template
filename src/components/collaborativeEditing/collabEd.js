import React, { Component } from 'react';
import CodeHolder from '../codeHolder/codeHolder';
import './collabEd.css';

class CollabEd extends Component {
  render() {
    return (
      <div className='collabEd'>
        <h1 className='collabEd-title'>Collaborative Editing</h1>
        <CodeHolder/>
      </div>
    )
  }
}

export default CollabEd